//
//  ViewController.swift
//  jegq6b
//
//  Created by user157768 on 7/9/19.
//  Copyright © 2019 UMSL. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    private var model: Model!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.isEnabled = false
    }
}

extension ViewController {
    private func attemptContactCreation() {
        if model.createContact(nameText: nameTextField.text, emailText: emailTextField.text, usernameText: usernameTextField.text, phoneText: phoneTextField.text, passwordText: passwordTextField.text){
             navigationController?.popViewController(animated: true)
        }
    }
}

extension ViewController {
    @IBAction private func addButtonTapped(_ sender: UIButton) {
        attemptContactCreation()
    }
    
    @IBAction private func textDidChange(_ sender: UITextField) {
        addButton.isEnabled = model.addButtonIsEnabled(nameText: nameTextField.text, emailText: emailTextField.text, usernameText: usernameTextField.text, phoneText: phoneTextField.text, passwordText: passwordTextField.text)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        attemptContactCreation()
        return true
    }
}

extension ViewController {

    func clearForm(){
        usernameTextField.text = ""
        nameTextField.text = ""
        emailTextField.text = ""
        phoneTextField.text = ""
        passwordTextField.text = ""
    }
}
