//
//  Register.swift
//  jegq6b
//
//  Created by user157768 on 7/9/19.
//  Copyright © 2019 UMSL. All rights reserved.
//

import Foundation

struct Register {
    let name: String
    let email: String
    let username: String
    let phone: String
    let password: String
}
