//
//  Model.swift
//  jegq6b
//
//  Created by user157768 on 7/9/19.
//  Copyright © 2019 UMSL. All rights reserved.
//

import Foundation


protocol ModelDelegate: class {
    func add(contact: Register)
}

final class Model {
    
    private weak var delegate: ModelDelegate?
    
    init(delegate: ModelDelegate) {
        self.delegate = delegate
    }
    
    func createContact(nameText: String?, emailText: String?, usernameText: String?, phoneText: String?, passwordText: String?) -> Bool {
        guard
            let name = nameText,
            !name.isEmpty,
            let email = emailText,
            !email.isEmpty,
            let username = usernameText,
            !username.isEmpty,
            let phone = phoneText,
            !phone.isEmpty,
            let password = passwordText,
            !password.isEmpty
        else {
            return false
        }
        let contact = Register(name: name, email: email, username: username, phone: phone, password: password)
        delegate?.add(contact: contact)
        return true
    }
    
    
    func addButtonIsEnabled(nameText: String?, emailText: String?, usernameText: String?, phoneText: String?, passwordText: String?) -> Bool {
        guard
            let nameText = nameText,
            !nameText.isEmpty,
            let emailText = emailText,
            !emailText.isEmpty,
            let usernameText = usernameText,
            !usernameText.isEmpty,
            let phoneText = phoneText,
            !phoneText.isEmpty,
            let passwordText = passwordText,
            !passwordText.isEmpty
            else {
                return false
        }
        return true
    }
}
