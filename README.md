# IOS-Project3

Project 3: More UIKit Elements and Delegation
You will create your own Xcode project for this assignment and then follow the instructions for submission when you're ready to upload it.

(Please note: Your last submission will be what is graded, whenever it is turned in. So if you have your project done before the due date, don't submit it and then submit again after the due date with extra credit stuff. If you want me to look at something cool that you did after the due date, just email me.)

## Requirements
Create a form that an app would use to "sign up" a new user. The interface can look however you would like (it doesn't have to be pretty--we'll work on pretty later), but the project must meet the following requirements.

1) The user interface should have UITextFields for name, username, password, email address, and phone number. The keyboard should properly resign when pressing Done/Return for each text field. There should UILabels for the "title" of each text field, so the user knows what kind of information to type in. There should be placeholder text in each text field. Secure text entry must be used on the password text field, and a suitable type of keyboard should be used for each of the text fields respectively.

2) An image view with an image (stored in Assets.xcassets) must be present on the screen. Think of it like an app logo or icon or really whatever you want/can find. 

3) There should be an "Add" button at the bottom of the form which will create the user in your Model and then clear all the text fields. It should only be enabled when the input passes validation. 

4) You can add whatever input validation you would like for the text fields. The only REQUIRED input validation is: i) that the password be at least 7 characters long, ii) that the phone number does not contain any non-numeric characters, and iii) that the rest of the fields have at least 3 characters. (You do not have to validate that it is a correct email address or phone number, though you can if you like).

5) Research UITextView, a new component that we have not gone over in class, and place one in the bottom of the third of your screen. Disable editing on it. The text view should have a description of each user in the app on a separate line. (So for example when I press the "Add" button, a new user is added to the text view on a new line.) The description should have all the fields of the user in it.

MVC
You need to use the Model-View-Controller pattern for this assignment. Put DOMAIN logic in your Model, and PRESENTATION and COORDINATION logic (enabling buttons, clearing text fields) in your View Controller.

### Specific notes on this:

- Put your input validation logic in the model. For example, you might make methods like: `isValidPassword(text: String) -> Bool`.

- When using delegation with UIKit components, make your View Controller the delegate. 

## Extra Credit
Up to 5 points of extra credit are available for getting creative with the project and adding additional features or capabilities. Describe any additions in the COMMENTS on your submission in Canvas.

Using Autolayout or adding in better input validation are examples of the kind of thing you could do. You could also make it to where hitting Enter/Return on the keyboard immediately brings focus to the next text field. Or you can think of something else cool or fun to do.

If you add extra stuff, just make sure you still meet all the requirements. And get the base project done first and make sure you have a copy of it before starting on anything extra!

Notes for Layout
1.We will be going over stack views and auto-layout this week. So, I will require that your apps be free of auto-layout warnings for this project. I am only concerned about portrait-orientation so please switch landscape off in your project file to save yourself some trouble.

2. Just design for the iPhone 8 in portrait mode. So, use the iPhone 8 Simulator when testing, and in Interface Builder, select "View as iPhone 8" for your storyboarding.

 

Instructions for Submission (same as last time)
As with all projects, your project should run on Xcode 10.2.1 and be free of compile-time errors and crashes (run-time errors). Those will have significant deductions.
You should also not have any compile-time warnings (in yellow).
When you're ready to submit, rename the folder containing the Xcode project  to your UMSL user id (e.g. 'gmhz7b'). Then right click on that folder and select the option that should say "Compress gmhz7b" (or whatever your user id is). Finally upload that zip file here.
Make sure you give yourself enough time to actually make the submission before the deadline.
It's always better to submit something than nothing!